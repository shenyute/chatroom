# coding: utf-8
import redis

class Storage:
    def __init__(self, host: str, port: int):
        self.client = redis.Redis(host=host, port=port, db=0)

    """
    Note: bucket is the prefix to identify object usage,
    and max file size is 500MB for redis limitation
    """
    def save(self, bucket: str, file_name: str, expire_seconds: int):
        """save file to bucket with file_name"""
        with open(file_name, "rb") as file:
            data = file.read()
            k = bucket + "-" + file_name
            self.client.set(k, data)
            self.client.expire(k, expire_seconds)

    def delete(self, bucket: str, file_name: str) -> bool:
        """delete file of bucket"""
        k = bucket + "-" + file_name
        self.client.delete(k)

    def get(self, bucket: str, file_name: str) -> bytes:
        """get file from bucket"""
        k = bucket + "-" + file_name
        return self.client.get(k)
