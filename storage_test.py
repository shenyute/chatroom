import unittest
import storage
import time

class StorageTestCase(unittest.TestCase):
  def test_simple_expire(self):
    s = storage.Storage('localhost', 7777)
    f = open("o", "w")
    body = "oooo"
    f.write(body)
    f.close()
    # expire after 10s
    s.save("local", "o", 10)

    self.assertEqual(bytes(body, "utf8"), s.get("local", "o"))
    time.sleep(5)
    self.assertEqual(bytes(body, "utf8"), s.get("local", "o"))
    time.sleep(5)
    # now should be expired
    self.assertEqual(None, s.get("local", "o"))

if __name__ == '__main__':
  unittest.main()
